function init () {
  // Keep phone awake while stopwatch is running
  if (window.plugins && window.plugins.insomnia) {
    console.log('Enable Keep Awake')
    window.plugins.insomnia.keepAwake()
  } else {
    console.log('Keep Awake not enabled')
  }

}

module.exports = init
