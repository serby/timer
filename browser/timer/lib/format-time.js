const padded = '00'

function pad (str) {
  if (str === undefined) return padded
  return (padded + str).slice(-padded.length)
}

function format (time) {
  var m = Math.floor(time / 60000)
    , s = Math.floor((time % 60000) / 1000)
    , h = Math.floor((time % 1000) / 10)

  return pad(m) + ':' + pad(s) + '.' + pad(h)
}

module.exports = format
