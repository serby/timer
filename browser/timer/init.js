var Collection = require('./collections/collection')
  , initController = require('./controllers/controller')

function init (serviceLocator) {

  var collection = new Collection(serviceLocator)

  initController(serviceLocator, collection)
}

module.exports = init
