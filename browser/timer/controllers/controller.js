var IndexView = require('../views/index')
  , Model = require('../models/model')
  , copy = require('../lib/clipboard-copier')

function controller (serviceLocator) {

  var model = new Model(serviceLocator)
    , indexView = new IndexView(serviceLocator, model)
  indexView.on('start', model.start.bind(model))
  indexView.on('split', model.split.bind(model))
  indexView.on('reset', model.reset.bind(model))
  indexView.on('stop', model.stop.bind(model))

  indexView.on('copy', function () {
    copy(model.toString())
  })

  window.$('#app').html(indexView.render().$el)

}

module.exports = controller
