'use strict'
var View = require('ventnor')
  , compileJade = require('browjadify-compile')
  , template = compileJade(__dirname + '/../templates/index.jade')
  , dragon = require('../lib/dragon')
  , format = require('../lib/format-time')

class IndexView extends View {

  constructor (serviceLocator, model) {
    super(serviceLocator, model)
    View.apply(this, arguments)
    this.model = model
    this.model.on('change:splits', this.render.bind(this))

    this.model.on('change:state', state => {
      this.toggleButtonVisibility(state)
    })
  }

  toggleButtonVisibility (state) {
    let $startButton = this.$el.find('.js-btn-start')
      , $stopButton = this.$el.find('.js-btn-stop')
      , $splitButton = this.$el.find('.js-btn-split')
      , $resetButton = this.$el.find('.js-btn-reset')

    switch (state) {
      case 2: // Running
        $startButton.addClass('is-vhidden')
        $stopButton.removeClass('is-vhidden')
        $splitButton.removeClass('is-vhidden')
        $resetButton.addClass('is-vhidden')
        break
      case 3: // Stopped
        $startButton.removeClass('is-vhidden')
        $stopButton.addClass('is-vhidden')
        $splitButton.addClass('is-vhidden')
        $resetButton.removeClass('is-vhidden')
        break
      case 1: // Zero
        $startButton.removeClass('is-vhidden')
        $stopButton.addClass('is-vhidden')
        $splitButton.addClass('is-vhidden')
        $resetButton.addClass('is-vhidden')
        break
    }
  }

  render () {
    if (this.currentDragon) this.currentDragon.cleanUp()
    this.currentDragon = dragon(template({ model: this.model, format: format }), this.model)
    this.$el.empty().append(this.currentDragon)
    this.$el.find('.js-btn-start').on('click', this.emit.bind(this, 'start'))
    this.$el.find('.js-btn-split').on('click', this.emit.bind(this, 'split'))
    this.$el.find('.js-btn-reset').on('click', this.emit.bind(this, 'reset'))
    this.$el.find('.js-btn-stop').on('click', this.emit.bind(this, 'stop'))
    this.$el.find('.js-btn-copy').on('click', this.emit.bind(this, 'copy'))
    this.toggleButtonVisibility(this.model.get('state'))
    return this
  }

}

module.exports = IndexView
