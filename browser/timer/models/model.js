'use strict'
var Merstone = require('merstone')
  , format = require('../lib/format-time')

const STATES =
  { ZERO: 1
  , RUNNING: 2
  , STOPPED: 3
  }

class Model extends Merstone {
  constructor (serviceLocator) {
    super()

    this.timerInterval = null
    Merstone.call(this, serviceLocator
      , { start: 0
        , time: 0
        , state: STATES.ZERO
        , formattedTime: null
        , formattedSplit: null
        , splitStart: 0
        , splitTime: 0
        , splits: []
      })

    this.reset()
  }

  tick () {
    let now = Date.now()
      , time = now - this.get('start')
      , splitStart = this.get('splitStart')
      , splitTime = now - splitStart

    this.set('time', time)
    this.set('formattedTime', format(time))
    this.set('splitTime', splitTime)
    this.set('formattedSplit', format(splitTime))
  }

  reset () {
    // if ([ STATES.STOPPED ].indexOf(this.get('state')) === -1) return
    clearInterval(this.timerInterval)
    this.set('state', STATES.ZERO)
    this.set('start', 0)
    this.set('time', 0)
    this.set('formattedTime', format(0))

    this.set('splitStart', 0)
    this.set('splitTime', 0)
    this.set('formattedSplit', format(0))
    this.set('splits', [])
  }

  start () {
    let now = Date.now()
    this.set('start', now - this.get('time'))
    this.set('splitStart', now - this.get('splitTime'))

    if ([ STATES.ZERO, STATES.STOPPED ].indexOf(this.get('state')) === -1) return
    this.timerInterval = setInterval(this.tick.bind(this), 50)
    this.set('state', STATES.RUNNING)
  }

  split () {
    if (this.get('state') !== STATES.RUNNING) return

    let splits = this.get('splits')
    splits.push(this.get('splitTime'))
    this.set('splits', splits)

    this.set('splitStart', Date.now())
    this.set('splitTime', 0)
  }

  stop () {
    if (this.get('state') !== STATES.RUNNING) return
    clearInterval(this.timerInterval)
    this.set('state', STATES.STOPPED)
  }

  toString () {
    var splits = this.get('splits')
      , output = 'Time: ' + this.get('formattedTime') + '\nSplit: ' + this.get('formattedSplit')

    if (splits.length > 0) {
      output += '\nSplits:\n' + this.get('splits').map(format).join('\n')
    }
    return output
  }

}

Model.STATES = STATES

module.exports = Model
