# Startup Image
convert -size '480x480' -thumbnail '320x480^' -gravity Center -extent 320x480 -strip $1 $2/Default~iphone.png
convert -size '640x960' -thumbnail '640x960^' -gravity Center -extent 640x960 -strip $1 $2/Default@2x~iphone.png
convert -size '1024x1024' -thumbnail '768x1024^' -gravity Center -extent 768x1024 -strip $1 $2/Default-Portrait~ipad.png
convert -size '2048x2048' -thumbnail '1536x2048^' -gravity Center -extent 1536x2048 -strip $1 $2/Default-Portrait@2x~ipad.png
convert -size '1024x1024' -thumbnail '2048x768^' -gravity Center -extent 1024x768 -strip $1 $2/Default-Landscape~ipad.png
convert -size '2048x2048' -thumbnail '2048x1536^' -gravity Center -extent 2048x1536 -strip $1 $2/Default-Landscape@2x~ipad.png
convert -size '1136x1136' -thumbnail '640x1136^' -gravity Center -extent 640x1136 -strip $1 $2/Default-568h@2x~iphone.png
convert -size '1136x1136' -thumbnail '750x1334^' -gravity Center -extent 750x1334 -strip $1 $2/Default-667h.png
convert -size '2208x2208' -thumbnail '1242x2208^' -gravity Center -extent 1242x2208 -strip $1 $2/Default-736h.png
convert -size '2208x2208' -thumbnail '2208x1242^' -gravity Center -extent 2208x1242 -strip $1 $2/Default-Landscape-736h.png
