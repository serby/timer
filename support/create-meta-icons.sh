# Icon

convert -resize '1000x1000' -strip $1 $2/meta-icon-1000x1000.jpg
convert -resize '180x180' -strip  $1 $2/apple-touch-icon-precomposed-180x180.png
convert -resize '120x120' -strip $1 $2/apple-touch-icon-precomposed-120x120.png
convert -resize '167x167' -strip $1 $2/apple-touch-icon-precomposed-167x167.png
convert -resize '152x152' -strip $1 $2/apple-touch-icon-precomposed-152x152.png
convert -resize '144x144' -strip $1 $2/microsoft-pinned-tile-image-144x144.png
convert -resize '144x144' -strip $1 $2/apple-touch-icon-precomposed-144x144.png
convert -resize '114x114' -strip $1 $2/apple-touch-icon-precomposed-114x114.png
convert -resize '100x100' -strip $1 $2/apple-touch-icon-precomposed-100x100.png
convert -resize '80x80' -strip $1 $2/apple-touch-icon-precomposed-80x80.png
convert -resize '60x60' -strip $1 $2/apple-touch-icon-precomposed-60x60.png
convert -resize '87x87' -strip $1 $2/apple-touch-icon-precomposed-87x87.png
convert -resize '76x76' -strip $1 $2/apple-touch-icon-precomposed-76x76.png
convert -resize '72x72' -strip $1 $2/apple-touch-icon-precomposed-72x72.png
convert -resize '58x58' -strip $1 $2/apple-touch-icon-precomposed-58x58.png
convert -resize '57x57' -strip $1 $2/apple-touch-icon-precomposed-57x57.png
convert -resize '50x50' -strip $1 $2/apple-touch-icon-precomposed-50x50.png
convert -resize '40x40' -strip $1 $2/apple-touch-icon-precomposed-40x40.png
convert -resize '29x29' -strip $1 $2/apple-touch-icon-precomposed-29x29.png
convert -resize '16x16' -strip $1 $2/favicon-16.png
convert -resize '32x32' -strip $1 $2/favicon-32.png
convert -resize '16x16' -strip $1 $2/favicon.ico
