var urlFormatter = require('../url-formatter')
  , extend = require('lodash.extend')

function staticPage (serviceLocator, template, locals) {
  return function (req, res) {
    res.send(template(
      extend({ config: serviceLocator.config
      , formattedUrls: urlFormatter(req)
      , meta: serviceLocator.config.meta
      }, locals || {})
    ))
  }
}

module.exports = staticPage
