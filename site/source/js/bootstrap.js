module.exports = bootstrap

var components =
  [ require('../../../browser/general/init')
  , require('../../../browser/timer/init') ]

function bootstrap (serviceLocator) {
  components.forEach(function (component) {
    component(serviceLocator)
  })
}
