module.exports = bundle

var browserify = require('browserify')
  , browjadify = require('browjadify')
  , babelify = require('babelify')

function bundle (entry, noParse) {

  var b = browserify(entry
    , { noParse: noParse
      , debug: true
      , insertGlobals: true
      , cache: {}
      , packageCache: {}
      , fullPaths: true
      })

  b
    .transform(babelify, { presets: [ 'es2015' ] })
    .transform(browjadify({ noParse: noParse, quiet: true }))

  return b

}
